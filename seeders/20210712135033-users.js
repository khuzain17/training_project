'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('users', [{
      email: "michael.lawson@reqres.in",
      password: null,
      first_name: "Michael",
      last_name: "Lawson",
      avatar: "https://reqres.in/img/faces/7-image.jpg",
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      email: "lindsay.ferguson@reqres.in",
      password: null,
      first_name: "Lindsay",
      last_name: "Ferguson",
      avatar: "https://reqres.in/img/faces/8-image.jpg",
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      email: "tobias.funke@reqres.in",
      password: null,
      first_name: "Tobias",
      last_name: "Funke",
      avatar: "https://reqres.in/img/faces/9-image.jpg",
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      email: "byron.fields@reqres.in",
      password: null,
      first_name: "Byron",
      last_name: "Fields",
      avatar: "https://reqres.in/img/faces/10-image.jpg",
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      email: "george.edwards@reqres.in",
      password: null,
      first_name: "George",
      last_name: "Edwards",
      avatar: "https://reqres.in/img/faces/11-image.jpg",
      createdAt: new Date(),
      updatedAt: new Date()
    }])
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};