const express = require("express");
const router = express.Router();

const controller = require("../controllers/controller");

const { users,Twit } = require("../models");

var checkAuthentication = (req, res, next) => {
  // console.log(req.user)
    if (req.isAuthenticated()) {
      return next();
    } else {
      res.redirect("/login");
    }
  };

  var checkvalue = (req, res, next) => {
    console.log(req.body.title)
    const title = req.body.title;
    const description = req.body.description;

    // return next();
      if (title == '' || description == '' || title == ' ' || description == ' ') {
        // const a = document.getElementById('')
        // console.log("=============================")
        res.redirect("/showpost");
      } else {
        return next();
      }
    };
  

router.get("/showUsers", controller.showUsers);

router.get("/", controller.getHomePage);
router.get("/register", controller.getRegisterPage);
router.get("/login", controller.getLoginPage);
router.get("/dashboard", checkAuthentication, controller.getDashboardPage);
router.get("/logout", controller.logout);
router.get("/update", controller.getUpdatePage);
router.get("/showpost",checkAuthentication, controller.getPostPage);
router.get("/post", checkAuthentication, controller.getPostPage);


router.post("/post", checkAuthentication, checkvalue, controller.post);
router.post("/register", controller.register);
router.post("/login", controller.login);
router.post("/update", controller.update);
router.get('/editwit/:id', checkAuthentication, controller.editwitpage)
router.post('/editwit', checkAuthentication, controller.editwit)
router.post('/pic', checkAuthentication, controller.pic)

// router.post('/deltwit', checkAuthentication, controller.deltwit)

router.post('/deltwit', checkAuthentication, (req, res) => {
  // console.log(req.user)
  console.log(req.body)
  Twit.findOne({
    where: {
      id: req.body.id
    },
    include: [
      {
          model: users
      }
    ] 
  })
  .then((twit) => {
    if (twit.user.dataValues.id === req.user.id){
      Twit.destroy({
        where: {
            id: req.body.id
        }
      })
      res.redirect("/showpost")
    }
    else {res.redirect("/showpost")}
  })
})



module.exports = router;