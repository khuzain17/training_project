const { users,Twit } = require("../models");
const bcrypt = require("bcrypt");
var passport = require("passport");
const { render } = require("ejs");

class Controller {
  static showUsers(req, res) {
    const usersArr = [];
    users.findAll({}).then((users) => {
      for (const user of users) {
        usersArr.push({
          email: user.dataValues.email,
          fName: user.dataValues.first_name,
          lName: user.dataValues.last_name,
        });
      }

      //   console.log(usersArr)
      res.render("show", {
        data: usersArr,
      });
    });
  }

  static register(req, res) {
    // console.log(req.body)
    const email = req.body.email;
    const password = bcrypt.hashSync(req.body.password, 10);
    const first_name = req.body.first_name;
    const last_name = req.body.last_name;
    const avatar = req.body.avatar;

    users.create({
        email: email,
        password: password,
        first_name: first_name,
        last_name: last_name,
        avatar: avatar,
      })
      .then((user) => {
        // console.log(user);
        res.render("login");
      })
      .catch((err) => {
        res.render("register");
      })
  }

  static update(req, res) {
    // console.log(req.body)
    // const email = req.body.email;
    const password = bcrypt.hashSync(req.body.password, 10);
    // const first_name = req.body.first_name;
    // const last_name = req.body.last_name;
    // const avatar = req.body.avatar;
    // console.log(req.body.password)
    // console.log("================")
    // console.log(req.user.password)
    users.update ({
      password: password
    }, {
      where: {
        id:req.user.id
      }
    })
    .then((user) => {
      // console.log("----------------------")
      // console.log(user)
      res.redirect("/dashboard")
    })
    
    
  }

  static login(req, res) {
    // console.log("login controller");
    // console.log(req.body)
    passport.authenticate("local", {
      successRedirect: "/dashboard", // GET
      failureRedirect: "/login", // GET
    })(req, res);
  }

  static post(req, res) {
    // console.log("+++++++++++++++++++++++++++++++")
    // console.log(req.user.id)
    // console.log("++++++++++++++++++++++++++")

    // const title = req.body.title;
    // const description = req.body.description;
    // const usersId = req.user;

    Twit.create({
        title: req.body.title,
        description: req.body.description,
        userId: req.user.id
      })
      .then((twit) => {
        // console.log(twit);
        res.redirect("/showpost");
      })
      .catch((err) => {
        res.render("dashboard");
      })
  }

  // static getFormPost(req, res) {
  //   res.render("post");
  // }

  static getPostPage(req, res) {
    // users.hasMany(Twit)
    // users.belongsTo(Twit)
    // users.findAll({}).then((Twit) => {
    //   console.log(Twit)
    // })
    // console.log("++++++++++++++++++++++")
    // users.findOne({ where: { id: '1' } });
    // console.log("++++++++++++++++++++++")
    const twitAll = [];
    Twit.findAll({
      order: [
        ['id', 'DESC']
      ],
      include: [
        {
            model: users
        }
      ]
    }).then((Twit) => {
      for (const twits of Twit) {
        // console.log(twits.dataValues.userId)
        // const nam = users.findByPk(twits.dataValues.userId)
        // console.log("#########################")
        // console.log(twits.user.dataValues.first_name)
        // console.log("+++++++++++++++++++++++++++")
        twitAll.push({
          id: twits.dataValues.id,
          title: twits.dataValues.title,
          description: twits.dataValues.description,
          fname: twits.user.dataValues.first_name,
          lname: twits.user.dataValues.last_name,
          userid: twits.user.dataValues.id,
          idbody: req.user.id
          // email: users.dataValues.last_name,
        });
        // console.log(twitAll)
      }

      //   console.log(usersArr)
      res.render("showpost", {
        data: twitAll,
      });
    });
  }

  static getHomePage(req, res) {
    res.render("index");
  }

  static getRegisterPage(req, res) {
    res.render("register");
  }

  static getLoginPage(req, res) {
    res.render("login");
  }
  
  static getDashboardPage(req, res) {
    // console.log(req)
    // console.log(req.user.first_name)
    res.render("dashboard", {
      fname: req.user.first_name,
      lname: req.user.last_name,
      pic: req.user.avatar
    })
    
  }

  static logout(req, res) {
    req.logout();
    res.redirect("/");
  }
  static getUpdatePage(req,res) {
    res.render("update");
  }

  static editwit(req, res) {
    Twit.update ({
      title: req.body.title,
      description: req.body.description
    }, {
      where: {
        id:req.body.id
      }
    })
    .then((user) => {
      // console.log("----------------------")
      // console.log(user)
      res.redirect("/showpost")
    })
  }

  static editwitpage (req, res) {
    Twit.findOne({
      where: {
        id: req.params.id
      },
      include: [
        {
            model: users
        }
      ] 
    })
    .then((twit) => {
      if (twit.user.dataValues.id === req.user.id){
        // console.log(req.user)
        // console.log(twit.dataValues)
        res.render('editwitpage', { 
          data: twit.dataValues
        })
      }
      else {res.redirect("/showpost")}
    })

  }

  static pic (req, res) {
    // console.log("+++++++++++++++++++++++++++")
    // console.log(req.body.pict)
    const pict = req.body.pict
    // console.log("+++++++++++++++++++++++++++")
    if (pict == "pink"){
    users.update ({
      avatar: "pink"
    }, {
      where: {
        id:req.user.id
      }
    })
    .then((user) => {
    users.findOne({
      where: 
          {
              id: req.user.id
          }
    })
    .then ((user) => {
        res.render("dashboard", {
        fname: req.user.first_name,
        lname: req.user.last_name,
        pic: user.avatar
      })
    })
  })
  }

  else if (pict == "red"){
    users.update ({
      avatar: "red"
    }, {
      where: {
        id:req.user.id
      }, returning:true,
    })
    .then((user) => {
      // console.log(user);
      // console.log("++++++++++++++++++");
    users.findOne({
      where: 
          {
              id: req.user.id
          }
    })
    .then ((user) => {
        res.render("dashboard", {
        fname: req.user.first_name,
        lname: req.user.last_name,
        pic: user.avatar
      })
    })
  })
  }

  else if (pict == "green"){
    users.update ({
      avatar: "green"
    }, {
      where: {
        id:req.user.id
      }
    })
    .then((user) => {

    users.findOne({
      where: 
          {
              id: req.user.id
          }
    })
    .then ((user) => {
      
        res.render("dashboard", {
        fname: req.user.first_name,
        lname: req.user.last_name,
        pic: user.avatar
      })
    })
  })
  }

  else if (pict == "blue"){
    users.update ({
      avatar: "blue"
    }, {
      where: {
        id:req.user.id
      }
    })
    .then((user) => {
    users.findOne({
      where: 
          {
              id: req.user.id
          }
    })
    .then ((user) => {
      
        res.render("dashboard", {
        fname: req.user.first_name,
        lname: req.user.last_name,
        pic: user.avatar
      })
    })
    })
  }
}

  // static deltwit (res, req) {
    // console.log("zaininnnnnnnnnnnnnnnnnnnn")
    // Twit.findOne({
    //   where: {
    //     id: req.params.id
    //   },
    //   include: [
    //     {
    //         model: users
    //     }
    //   ] 
    // })
    // .then((twit) => {
    //   if (twit.user.dataValues.id === req.user.id){
    //     Twit.destroy({
    //       where: {
    //           id: req.params.id
    //       }
    //     })
    //     res.render("dashboard")
    //   }
    //   else {res.redirect("/showpost")}
    // })
  // }

}

module.exports = Controller;

