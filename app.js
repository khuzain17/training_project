const express = require("express");
const path = require("path");
var passport = require("passport");
var LocalStrategy = require("passport-local").Strategy;
var session = require("express-session");
const bcrypt = require("bcrypt");

const port = process.env.PORT || 3000;

const router = require("./routes/router");
const { users } = require("./models");

const app = express();

app.use( express.static( "public" ) );
app.use(express.json());
app.use(express.urlencoded());
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

//Express session
app.use(
  session({
    secret: "secret",
    resave: true,
    saveUninitialized: true,
  })
);

// passport config
passport.use(
  new LocalStrategy(
    {
      usernameField: "email",
      passwordField: "password",
    },
    (email, password, done) => {
      // console.log('masuk passport')
      users.findOne({
          where: {
            email: email,
          },
        })
        .then((user) => {
          // console.log(user);
          if (user && user.dataValues) {
            const encryptedPassword = user.dataValues.password
  
            if (bcrypt.compareSync(password, encryptedPassword)) {
              return done(null, user)
            } else {
              return done(null, false)
            }
          } else {
            return done(null, false)
          }
        });
    }
  )
);
passport.serializeUser((user, done) => {
  done(null, user);
});
passport.deserializeUser((obj, done) => {
  done(null, obj);
});

// Passport middleware
app.use(passport.initialize());
app.use(passport.session());

app.use("/", router);

app.use((err, req, res, next) => {
  if (err) {
    console.log(err);
  }
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:3000`);
});

module.exports = app;
